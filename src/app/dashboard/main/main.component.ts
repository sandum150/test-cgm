import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {User} from '../../../models/user.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styles: []
})
export class MainComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  currentUser:User;

  ngOnInit() {
    this.currentUser = this.authService.currentUser;
  }
  onLogout(){
    this.authService.logOut();
    this.router.navigate(['/login'])
  }
}
