import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/user.model';
import {UsersService} from '../../services/users.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: []
})
export class RegisterComponent implements OnInit {


  /* model driven approach form */
  registerForm: FormGroup;
  validate = false;

  cities = [
    'New York',
    'Paris',
    'Iasi',
    'Prague',
    'Chisinau',
  ];

  constructor(private userService: UsersService, private route: Router) {
    this.registerForm = new FormGroup({
      userName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      city: new FormControl(null, Validators.required),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      passwordConfirm: new FormControl(null, [Validators.required, this.checkIfMatchingPasswords.bind(this)])
    })
  }

  ngOnInit() {
    this.registerForm.patchValue({'email': this.route.routerState.snapshot.root.queryParams.email})
  }

  onSubmitRegister(){
    if(this.registerForm.valid){
      const newUser = new User(
        this.registerForm.value.userName,
        this.registerForm.value.email,
        this.registerForm.value.city,
        this.registerForm.value.password
      ) ;
      this.userService.addUser(newUser);
    }else {
      this.validate = true;
    }
  }

  checkIfMatchingPasswords() {
    if (this.registerForm) {
      return this.registerForm.get('password').value === this.registerForm.get('passwordConfirm').value ? null : {notSame: true};
    }
  }

}
