import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services/users.service';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  constructor(private usersService: UsersService, private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit(f){
    const result  = this.usersService.findUser(f.value.email, f.value.password);
    if(!result){
      this.router.navigate(['/register'], {queryParams: {email: f.value.email}})
    }else {

      this.authService.login(result);
      this.router.navigate(['/dashboard'])
    }
  }

}
