import {User} from '../models/user.model';

export class AuthService{
  loggedIn = false;
  currentUser:User;

  isAuthenticated(){
    const promise = new Promise((resolve, reject) => {

      // emulate async auth
      setTimeout(() => {
        resolve(this.loggedIn);
      }, 500)
    })
    return promise
  }

  login(user: User){
    this.loggedIn = true;
    this.currentUser = user;
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  logOut(){
    this.loggedIn = false;
    this.currentUser = null;
    localStorage.removeItem('currentUser')
  }
}
