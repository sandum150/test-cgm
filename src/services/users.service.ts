import {Injectable} from '@angular/core';
import {User} from '../models/user.model';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root',
})

export class UsersService {

  constructor(private authService: AuthService){}

  users:User[] = [
    new User('admin', 'admin', 'Paris', 'admin')
  ];

  addUser(user:User){
    this.users.push(user);
    localStorage.setItem('users', JSON.stringify(this.users));
  }

  setUsers(){
    //set all users
    let localUsers = localStorage.getItem('users');
    if(localUsers){
      const users = <any[]>JSON.parse(localUsers);
      let allUsers:User[] = [];
      users.forEach(u => {
        allUsers.push(new User(u.name, u.email, u.city, u.password))
      });
      this.users = allUsers;
    }
  //  set current user
    let localCurrentUser = localStorage.getItem('currentUser');
    if(localCurrentUser){
      const currentUser = <any>JSON.parse(localCurrentUser);
      const localUser = new User(
        currentUser.name,
        currentUser.email,
        currentUser.city,
        currentUser.password,
      );
      this.authService.login(localUser);
    }
  }

  public findUser(email: string, password: string) {
    const user = this.users.find((u:User) => {
      return u.checkUserAndPassword(email, password)
    });

    return user || false
  }

}
